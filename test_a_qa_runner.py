from selenium import webdriver
# from selenium.webdriver.common.by import By
# from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
# import unittest, time, re
import unittest
from selenium.webdriver.chrome.options import Options
chrome_options = Options()
chrome_options.add_argument('--headless')
chrome_options.add_argument('--no-sandbox')
chrome_options.add_argument('--disable-dev-shm-usage')
# chrome_options.add_argument("--disable-extensions")

stage2_link = 'https://workforce-emspublic-stage2.synesis-sport.com'
user_login = 'test-wkf'
user_pass = 'test'
# chrome_options = Options()
# chrome_options.add_argument('--headless')
# chrome_options.add_argument('--no-sandbox')
# chrome_options.add_argument('--disable-dev-shm-usage')
# driver = webdriver.Chrome('/path/to/your_chrome_driver_dir/chromedriver',chrome_options=chrome_options)


class TestPublicLogin(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome('/home/ihar/Git_Work/test_one/test_one/driver/chromedriver',
                                       chrome_options=chrome_options)
        self.driver.implicitly_wait(10)
        self.base_url = stage2_link
        self.verificationErrors = []
        self.accept_next_alert = True

    def test_login(self):
        driver = self.driver
        driver.get(self.base_url + '')
        driver.find_element_by_id('username').clear()
        driver.find_element_by_id('username').send_keys(user_login)
        driver.find_element_by_id('password').clear()
        driver.find_element_by_id('password').send_keys(user_pass)
        driver.find_element_by_name('login').click()

    def is_element_present(self, how, what):
        try:
            self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e:
            return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert.text
        finally:
            self.accept_next_alert = True

    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

